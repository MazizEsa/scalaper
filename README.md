# README #

A very rudimentary, un-optimize, hacky scrapper.

A scrapper in scala. Currently this scrapper can only scrap Google Test Blog "http://googletesting.blogspot.com/".
This script will scrap url and the title of the blog and store it into a file.

This script uses scopt to parse arguments and jsoup to parse web pages.
 
* 0.1Alpha

### Usage Notes ###

* How to use:
scalaper 0.1Alpha    
Usage: scalaper [options] 
                     
  -u <value> | --url <value>
        url is a String. Determine which url to scrap
  -d <value> | --depth <value>
        depth is an Integer. Determine the level of which to scrap
  -s <value> | --searchContext <value>
        searchContext is a String. Determine the keyword to be searched
Please report any bug 
                     
  --help             
        Will display this instruction

Use the url to point to the web page you want to scrap (in this case only google test blog)

Use depth to determine how many page to scrap..

Use search context to determine what type of keyword to be based on when scrapping


### Roadmap ###

Currently the roadmap is pretty sketchy. There are 2 options:
1. Making this script as a general scrapper for any web page.
2. Making this script / project a library to be used as scala scrapper.

TBD.

### License ###

Copyright 2014 Maziz

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.