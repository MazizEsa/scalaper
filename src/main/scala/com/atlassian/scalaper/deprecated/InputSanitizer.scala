package com.atlassian.scalaper.deprecated

/**
 * Created by mahmad on 9/26/14.
 */
class InputSanitizer private(val inputArguments: Array[String]) {

  def sanitizeInputs() = {

  }

  def checkInputs(): Either[IllegalArgumentException, Boolean] = {
    try {
      require(inputArguments.length > 0, "Must put at least 1 arguments")
      //TODO: Add checking for url and depth
      //TODO: Parse crawled web
      //TODO: Index to csv

      Right(true)
    }
    catch {
      case illegalArgumentException: IllegalArgumentException => Left(illegalArgumentException)
    }
  }

}

object InputSanitizer {
  def apply(inputArguments: Array[String]) = new InputSanitizer(inputArguments);
}
