package com.atlassian.scalaper.common

/**
 * Created by mahmad on 10/1/14.
 */
object CommandParams {
  val Url = "url"
  val ShortUrl = 'u'

  val Depth = "depth"
  val ShortDepth = 'd'

  val SearchContext = "searchContext"
  val ShortSearchContext = 's'
}
