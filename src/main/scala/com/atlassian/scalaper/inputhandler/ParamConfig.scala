package com.atlassian.scalaper.inputhandler

/**
 * Created by mahmad on 9/30/14.
 */
case class ParamConfig(url: String, depth: Int, searchContext: String)
