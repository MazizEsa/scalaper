/*
 * Copyright 2014 Maziz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.scalaper.inputhandler

import com.atlassian.scalaper.common.CommandParams

/**
 * Created by mahmad on 9/30/14.
 */
class CommandParser private(arguments: Array[String]) {
  private val _mappedCommands: scala.collection.mutable.Map[String, String] = scala.collection.mutable.Map[String, String]()

  def mappedCommands = _mappedCommands

  val commandParser = new scopt.OptionParser[ParamConfig]("scalaper") {
    head("scalaper", "0.1Alpha")
    opt[String](CommandParams.ShortUrl, CommandParams.Url) required() action { (urlInput, config) =>
      config.copy(url = urlInput)
    } text ("url is a String. Determine which url to scrap")
    opt[Int](CommandParams.ShortDepth, CommandParams.Depth) action { (depthInput, config) =>
      config.copy(depth = depthInput)
    } text ("depth is an Integer. Determine the level of which")
    opt[String](CommandParams.ShortSearchContext, CommandParams.SearchContext) required() action { (searchContextInput, config) =>
      config.copy(searchContext = searchContextInput)
    } text ("searchContext is a String. Determine the keyword to be searched")
    note("Please report any bug \n")
    help("help") text ("Will display this instruction")
  }

  def showMessage() = {
    commandParser.showUsage
  }

  def parseArguments() = {
    commandParser.parse(arguments, ParamConfig("", 0, "")) map {
      ParamConfig => handleArguments(arguments)
    } getOrElse {

    }
  }

  private def handleArguments(arguments: Array[String]) = {
    mappedParamArrayToMap(arguments)
  }

  def mappedParamArrayToMap(arguments: Array[String]) = {
    for {
      (argument, index) <- arguments.zipWithIndex
    } yield {
      argument match {
        case "--url" => mappedCommands += (CommandParams.Url -> arguments((index + 1)))
        case "--depth" => (mappedCommands += (CommandParams.Depth -> arguments((index + 1))))
        case "--searchContext" => (mappedCommands += (CommandParams.SearchContext -> arguments((index + 1))))
        case _ => //TODO: Do Nothing
      }

    }
  }
}

object CommandParser {
  def apply(arguments: Array[String]) = new CommandParser(arguments)
}
