package com.atlassian.scalaper

import com.atlassian.scalaper.common.CommandParams
import com.atlassian.scalaper.inputhandler.CommandParser
import com.atlassian.scalaper.pageparser.JsoupWrapper
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.Elements

import scala.collection.mutable.ListBuffer
import scala.xml.XML
;

/**
 * Created by mahmad on 9/26/14.
 */
object MainModule {

  def main(arguments: Array[String]): Unit = {
    val commandParser: CommandParser = CommandParser(arguments)
    commandParser.parseArguments()

    val mappedCommands: scala.collection.mutable.Map[String, String] = commandParser.mappedCommands

    if ((!mappedCommands.get(CommandParams.Url).getOrElse("").equalsIgnoreCase("") && (!mappedCommands.get(CommandParams.SearchContext).getOrElse("").equalsIgnoreCase("")))) {
      val filteredElementList: ListBuffer[Element] = startScrapping(mappedCommands, mappedCommands.get(CommandParams.Url).get)

      val prettyConvertedXML = constructXMLFile(filteredElementList)
      storeXMLIntoFile(prettyConvertedXML)
    }
  }

  def startScrapping(mappedCommands: scala.collection.mutable.Map[String, String], url: String): ListBuffer[Element] = {
    val filteredElementList: ListBuffer[Element] = ListBuffer[Element]()

    def scrappingCeption(currentDept: Int, url: String): Unit = {
      if (currentDept == 0) {

      } else {
        val jsoup: JsoupWrapper = new JsoupWrapper(url)
        val doc: Document = jsoup.connect()

        val elements: Elements = doc.select("h3").select("a")

        for (eachHeader <- 0 to (elements.size() - 1)) {
          if (elements.get(eachHeader).text().toLowerCase.contains(mappedCommands.get(CommandParams.SearchContext).get.toLowerCase)) {
            filteredElementList += (elements.get(eachHeader))
          }
        }
        val olderElement: Element = doc.select("#Blog1_blog-pager-older-link").first()
        scrappingCeption(currentDept - 1, olderElement.attr("href"))
      }


    }
    scrappingCeption(mappedCommands.get(CommandParams.Depth).getOrElse("1").toInt, url)
    filteredElementList
  }

  def constructXMLFile(filteredElementList: ListBuffer[Element]) = {
    //TODO: Able to extract below into an abstract class and traits. Output could also be mixin (pretty / not pretty)
    val baseXML = new StringBuilder
    baseXML ++= "<GoogleTestBlog>"

    var dataXML = new StringBuilder

    for {
      filteredElement <- filteredElementList
    } yield {
      dataXML ++= "<Data><Link>"
      dataXML ++= filteredElement.attr("href")
      dataXML ++= "</Link><Title>"
      dataXML ++= filteredElement.select("a").text()
      dataXML ++= "</Title></Data>"
    }

    baseXML ++= dataXML
    baseXML ++= "</GoogleTestBlog>"

    println(baseXML)

    val convertedXML = scala.xml.XML.loadString(baseXML.toString())
    val xmlFormatter = new scala.xml.PrettyPrinter(80, 2)

    val prettyConvertedXML = xmlFormatter.format(convertedXML)
    prettyConvertedXML
  }

  def storeXMLIntoFile(xml: String) = {
    XML.save("googleTestBlog", scala.xml.XML.loadString(xml.toString()), "UTF-8", false, null)
  }
}
