package com.atlassian.scalaper.pageparser

import org.jsoup.Jsoup
import org.jsoup.nodes.Document

/**
 * Created by mahmad on 10/1/14.
 */
class JsoupWrapper(url: String) {
  private var _document: Document = _

  def document = _document

  def document_=(doc: Document): Unit = _document = doc

  def connect() = {
    document = Jsoup.connect(url).get()
    document
  }
}
